# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/sys-devel/gcc/gcc-4.1.1-r3.ebuild,v 1.8 2007/01/08 22:51:51 opfer Exp $

PATCH_VER="1.9"
UCLIBC_VER="1.1"
PIE_VER="9.0.6"
PIE_GCC_VER="4.1.1"

ETYPE="gcc-compiler"

# arch/libc configurations known to be stable with {PIE,SSP}-by-default
SSP_STABLE="amd64 ppc ppc64 sparc x86"
SSP_UCLIBC_STABLE="ppc sparc x86"
PIE_GLIBC_STABLE="amd64 ppc ppc64 sparc x86"
PIE_UCLIBC_STABLE="mips ppc x86"

# arch/libc configurations known to be broken with {PIE,SSP}-by-default
# gcc-4 SSP is only available on FRAME_GROWS_DOWNWARD arches; so it's not
# available on pa, c4x, ia64, alpha, iq2000, m68hc11, stormy16
# (the options are parsed, but they're effectively no-ops).
# rs6000 has special handling to support SSP; ia64 may get the same:
# http://developer.momonga-linux.org/viewvc/trunk/pkgs/gcc4/gcc41-ia64-stack-protector.patch?revision=7447&view=markup&pathrev=7447
SSP_UNSUPPORTED="hppa sh ia64 alpha"
SSP_UCLIBC_UNSUPPORTED="${SSP_UNSUPPORTED}"
PIE_UCLIBC_UNSUPPORTED="alpha amd64 arm hppa ia64 m68k ppc64 s390 sh sparc"
PIE_GLIBC_UNSUPPORTED="hppa"

# These patches are obsoleted by stricter control over how one builds a hardened
# compiler from a vanilla compiler
GENTOO_PATCH_EXCLUDE="54_all_300-libstdc++-pic.patch 51_all_gcc-3.4-libiberty-pic.patch"

# whether we should split out specs files for multiple {PIE,SSP}-by-default
# and vanilla configurations.
SPLIT_SPECS=${SPLIT_SPECS-true}

inherit toolchain

DESCRIPTION="The GNU Compiler Collection.  Includes C/C++, java compilers, pie+ssp extensions, Haj Ten Brugge runtime bounds checking"

LICENSE="GPL-2 LGPL-2.1"
KEYWORDS="-* ~alpha ~amd64 ~arm hppa ~ia64 ~mips ppc ppc64 ~s390 ~sparc ~sparc-fbsd x86 ~x86-fbsd"

RDEPEND=">=sys-libs/zlib-1.1.4
	|| ( >=sys-devel/gcc-config-1.3.12-r4 app-admin/eselect-compiler )
	virtual/libiconv
	fortran? (
		>=dev-libs/gmp-4.2.1
		>=dev-libs/mpfr-2.2.0_p10
	)
	!build? (
		gcj? (
			gtk? (
				|| ( ( x11-libs/libXt x11-libs/libX11 x11-libs/libXtst x11-proto/xproto x11-proto/xextproto ) virtual/x11 )
				>=x11-libs/gtk+-2.2
				x11-libs/pango
			)
			>=media-libs/libart_lgpl-2.1
		)
		>=sys-libs/ncurses-5.2-r2
		nls? ( sys-devel/gettext )
		hardened? ( >=sys-libs/glibc-2.4 )
	)"
DEPEND="${RDEPEND}
	test? ( sys-devel/autogen dev-util/dejagnu )
	>=sys-apps/texinfo-4.2-r4
	>=sys-devel/bison-1.875
	ppc? ( >=${CATEGORY}/binutils-2.17 )
	ppc64? ( >=${CATEGORY}/binutils-2.17 )
	>=${CATEGORY}/binutils-2.15.94"
PDEPEND="|| ( sys-devel/gcc-config app-admin/eselect-compiler )"
if [[ ${CATEGORY} != cross-* ]] ; then
	PDEPEND="${PDEPEND} elibc_glibc? ( >=sys-libs/glibc-2.3.6 )"
fi

src_unpack() {
	gcc_src_unpack

	use vanilla && return 0

	[[ ${CHOST} == ${CTARGET} ]] && epatch "${FILESDIR}"/gcc-spec-env.patch

	# Fix cross-compiling
	epatch "${FILESDIR}"/4.1.0/gcc-4.1.0-cross-compile.patch

	[[ ${CTARGET} == *-softfloat-* ]] && epatch "${FILESDIR}"/4.0.2/gcc-4.0.2-softfloat.patch

	epatch "${FILESDIR}"/4.1.0/gcc-4.1.0-fast-math-i386-Os-workaround.patch
}
